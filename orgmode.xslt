<?xml version="1.0"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema" version="2.0">
  <xsl:output method="text" indent="yes" omit-xml-declaration="yes"/>
  <xsl:template match="/folders">
    <xsl:apply-templates select="folder"/>
  </xsl:template>
  <xsl:template match="folder">
    <xsl:text>
</xsl:text>
    <xsl:sequence select="string-join ((for $i in (1 to count (ancestor::folder) + 2) return '*'),'')"/>
    <xsl:value-of select="title"/>
    <xsl:text>
</xsl:text>
    <xsl:apply-templates select="folder"/>
    <xsl:apply-templates select="bookmark"/>
  </xsl:template>
  <xsl:template match="bookmark">
    <xsl:text>- [[</xsl:text>
    <xsl:value-of select="@href" disable-output-escaping="no"/>
    <xsl:text>][</xsl:text>
    <xsl:value-of select="title"/>
    <xsl:text>]]
</xsl:text>
  </xsl:template>
</xsl:stylesheet>
