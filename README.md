# Introduction

Needed a way to export some firefox bookmarks to asciidoc und used the opportunity to learn a bit on sqlite, xml, xslt, and asciidoc.

# Usage

Currenty, `exportbookmarks` supports asciidoc and org-mode format.

`./exportbookmarks.bash [ARGS] > file` 

### Command line options

  * `-p|--places`: path to firefox places.sqlite file you want to export
  * `-o|--orgmode`: to export to org-mode file format
  * `-a|--adoc`: to export to asciidoc file format
  
