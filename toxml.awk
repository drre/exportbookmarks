BEGIN {
    print "<?xml version=\"1.0\" encoding=\"utf-8\"?>"
    print "<xbel version=\"1.0\"> "
    LASTPARENTIDX=0
    print "<folders>"
}

function escapexml(str, res) {
    res=gensub("&","&amp;","g",str)
    res=gensub("<","&lt;","g",res)
    res=gensub(">","&gt;","g",res)
    res=gensub("\"","&quot;","g",res)
    res=gensub("'","&apos;","g",res)
    return(res)
}

function fixurl(str) {
    return gensub(/^ /,"",1,str)
}

function getdomain(url, result) {
    match(tolower(url),"(^[[:alnum:]]*://)?([[:alnum:]\\-\\.]+\\.[[:alnum:]]+)/(.*)", result)
    print result[2]
    return result[2]
}

function basename(file) {

    bname=gensub("^[a-zA-Z]*://","",1,file)
    return bname
  }

function dirname(file) {
    dname = gensub("/*[^/]*/*$", "", 1, file)
    if(dname == "") {
	if(substr(file, 1, 1) == "/") {
	    return "/"
	}
	else {
	    return "."
	}
    }
    else {
	return dname
    }  
}

NR>1 {

    while (LASTPARENTIDX!=0 &&
	   LASTPARENTIDS[LASTPARENTIDX] != $5) {
	print "</folder>"
	delete LASTPARENTIDS[LASTPARENTIDX]
	LASTPARENTIDX--
    }

  

  if ( $2  == 2 ) {
	print "<folder>"
	print "<title>", escapexml($6),"</title>"
	LASTPARENTIDX++
	LASTPARENTIDS[LASTPARENTIDX]=$1
    }
    
    
    if ($2 == 1) {
	print " <bookmark href = \"",escapexml(fixurl($7)),"\">"
	
#	    print " <title>", escapexml(basename(dirname($7))),"</title>"
	    print " <title>", escapexml(getdomain($7)),"</title>"
	
	print "</bookmark>"

    }

  
    
}

END {
      while (LASTPARENTIDX!=0) {
	print "</folder>"
	LASTPARENTIDX--
    }
      print "</folders>"
      print "</xbel>"
}
